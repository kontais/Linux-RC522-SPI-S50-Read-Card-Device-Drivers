KERN_DIR = /opt/2440/linux-3.0.8

all:
	make -C $(KERN_DIR) M=`pwd` modules 
	arm-linux-gcc -o rc522_test rc522_test.c
clean:
	make -C $(KERN_DIR) M=`pwd` modules clean
	rm -rf modules.order
	rm rc522_test

obj-m += gt2440_rc522.o


