/*
 *  /linux-3.0.8/drivers/spi/gt2440_rc522.h
*/
#ifndef __GT2440_RC522_H
#define __GT2440_RC522_H
#include <linux/types.h>

/***** driver data structure *****/
struct gt2440_rc522_platdata{
	unsigned long	power_pin;


	int (*setup_pin)(void);
	int (*exit_pin)(void);
};

/***** driver data structure *****/
#define INFO_BUFFER_SIZE 32

struct gt2440_rc522_info{
	struct list_head	device_entry;
	struct spi_device	*spi_dev;
	
	unsigned		users;
	dev_t			devt;
	spinlock_t		spi_lock;
	struct mutex		buf_lock;

	u8			tx_buffer[INFO_BUFFER_SIZE];
	u8			rx_buffer[INFO_BUFFER_SIZE];
};



/***** rc522 cmd and s50 cmd and regs *****/
#define PCD_IDLE            0x00    /* Cancle the current command */
#define PCD_AUTHENT         0x0E    /* Authentication key */
#define PCD_RECEIVE         0x08    /* Receive data */
#define PCD_TRANSMIT        0x04    /* To send data */
#define PCD_TRANSCEIVE      0x0C    /* To send and receive data */
#define PCD_RESETPHASE      0x0F    /* Reset */ 
#define PCD_CALCCRC         0x03    /* CRC calculation */

#define PICC_REQIDL         0x26    /* Looking for a IC card in the area of antenna, that 
                                       didn't enter a dormant state the area of IC card */
#define PICC_REQALL         0x52    /* Looking for all IC card in the area of antena */
#define PICC_ANTICOLL1      0x93    /* Anticollision */
#define PICC_ANTICOLL2      0x95    /* Anticollision */
#define PICC_AUTHENT1A      0x60    /* Authentication A key */
#define PICC_AUTHENT1B      0x61    /* Authentication B key */
#define PICC_READ           0x30    /* Read block */
#define PICC_WRITE          0xA0    /* Write block */
#define PICC_DECREMENT      0xC0    /* Deductions */
#define PICC_INCREMENT      0xC1    /* Rechange */
#define PICC_RESTORE        0xC2    /* The block of data transferrend to the buffer */
#define PICC_TRANSFER       0xB0    /* Save a data in a buffer */
#define PICC_HALT           0x50    /* dormancy */


#define DEF_FIFO_LENGTH     64      /* FIFO size = 64byte */
#define MAXRLEN             18      /* The maximum length of data received */

/***** PAGE 0 *****/
#define RFU00               0x00
#define COMMAND_REG         0x01
#define COM_IEN_REG         0x02
#define DIVL_EN_REG         0x03
#define COM_IRQ_REG         0x04
#define DIV_IRQ_REG         0x05
#define ERROR_REG           0x06
#define STATUS1_REG         0x07
#define STATUS2_REG         0x08
#define FIFO_DATA_REG       0x09
#define FIFO_LEVEL_REG      0x0A
#define WATER_LEVEL_REG     0x0B
#define CONTROL_REG         0x0C
#define BIT_FRAMING_REG     0x0D
#define COLL_REG            0x0E
#define RFU0F               0x0F

#define RFU10               0x10
#define MODE_REG            0x11
#define TX_MODE_REG         0x12
#define RX_MODE_REG         0x13
#define TX_CONTROL_REG      0x14
#define TX_AUTO_REG         0x15
#define TX_SEL_REG          0x16
#define RX_SEL_REG          0x17
#define RX_THRESHOLD_REG    0x18
#define DEMOD_REG           0x19
#define RFU1A               0x1A
#define RFU1B               0x1B
#define MIFARE_REG          0x1C
#define RFU1D               0x1D
#define RFU1E               0x1E
#define SERIAL_SPEED_REG    0x1F

#define RFU20               0x20  
#define CRC_RESULT_REG_M    0x21
#define CRC_RESULT_REG_L    0x22
#define RFU23               0x23
#define MOD_WIDTH_REG       0x24
#define RFU25               0x25
#define RF_CFG_REG          0x26
#define GS_NREG             0x27
#define CWGS_CFG_REG        0x28
#define MOD_GS_CFG_REG      0x29
#define T_MODE_REG          0x2A
#define T_PRESCALER_REG     0x2B
#define T_RELOAD_REG_H      0x2C
#define T_RELOAD_REG_L      0x2D
#define T_COUN_VALUE_REGH   0x2E
#define T_COUN_VALUE_REGL   0x2F

#define RFU30               0x30
#define TEST_SEL1_REG       0x31
#define TEST_SEL2_REG       0x32
#define TEST_PIN_EN_REG     0x33
#define TEST_PIN_VALUE_REG  0x34
#define TEST_BUS_REG        0x35
#define AUTO_TEST_REG       0x36
#define VERSION_REG         0x37
#define ANALOG_TEST_REG     0x38
#define TEST_DAC1_REG       0x39  
#define TEST_DAC2_REG       0x3A   
#define TEST_ADC_REG        0x3B   
#define RFU3C               0x3C   
#define RFU3D               0x3D   
#define RFU3E               0x3E   
#define RFU3F               0x3F

#define MI_OK               0    
#define MI_NOTAGERR         (-1)
#define MI_ERR              (-2)


/***** device name  *****/

#define SPICHAR_MAJOR			156	/* assigned */
#define N_SPI_MINORS			32	/* ... up to 256 */

#define SPI_DEVICE_NAME "rc522"


/***** rc522 ops function declare ****/

static long gt2440_rc522_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
static int gt2440_rc522_open(struct inode *inode, struct file *filp);
static int gt2440_rc522_release(struct inode *inode, struct file *filp);


/***** debug  *****/
#define GT2440_RC522_DEBUG
#ifdef GT2440_RC522_DEBUG
#define dprintk printk
#else
#define dprintk(...)
#endif

#define GT2440_RC522_TEST


#endif
