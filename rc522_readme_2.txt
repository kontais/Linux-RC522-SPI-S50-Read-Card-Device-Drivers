
Linux RC522 SPI S50 Read Card Device Drivers驱动使用说明：
1，linux SPI Master 框架下的rc522 读卡器驱动。
在S3C2440和S5PV210开发板测试通过,但是不具体依赖于开发板，只要支持spi master驱动，当然Pin连接要匹配你的系统。
修正了内核spi子系统自带的bug（多次加载提示选择线忙的问题）。并且完全不依赖bsp文件（mach-xxxx.c）了，
用户可以直接在驱动文件里面修改修改参数（spi 模式，通信速度等等），完全独立编译运行。

2，应用程序可以使用ioctl完成相关操作，用户接口简单方便。
在驱动程序里面实现了所有操作命令，使用一个结构体struct rc522_ioc_transfer{}和应用程序进行通信。
用户只需要设置结构体struct rc522_ioc_transfer{}的相关成员即可完成所有操作，最大幅度降低用户编程。
对控制块的操作（密码区，控制区）也被封装起了，可以避免因为用户的误操作而损伤卡，
也方便用户对控制块进行单独具体的设置（读，写密码等等）。
用户只需要在设置结构体有关（命令）信息，再调用ioctl就可以了，
可能需要的返回数据（进行寻卡，读操作时）也在这个结构体的缓存数组成员(txrx_buf[])里面。

3，具体操作命令：
3-1，RC522_IOC_REQ_CARD命令：
只需一条命令完成所有寻卡过程。
调用ioctl后，RC522驱动自动完成整个寻卡->。。。->选择卡操作，成功后返回选中卡所有信息（卡类型，id号，容量，厂商信息）。
3-2，RC522_IOC_READ_CARD，RC522_IOC_WRITE_CARD命令：
设置好要操作的扇区，块，密码后，调用ioctl，RC522驱动自动完成crc校验，密码校验，读写卡操作，成功后返回块的数据（读操作时）。
3-3，RC522_IOC_READ_KEYA，RC522_IOC_READ_KEYB，
	RC522_IOC_WRITE_KEYA，RC522_IOC_WRITE_KEYB命令：
设置好要操作的扇区，密码后，要设置的新密码（修改密码时），
调用ioctl，RC522驱动自动完成crc校验，密码校验，读密码，写新密码（写密码时）操作，
3-4，RC522_IOC_READ_CTRL，RC522_IOC_WRITE_CTRL命令：
设置好要操作的扇区，密码后，要设置的新密码（修改密码时），
调用ioctl，同上面，RC522驱动自动完成所有操作，
修改控制区时，建议可以先可以参考S50卡说明书进行设置，
3-5，用户可以根据具体要求在驱动中添加自己想要的命令，以更适合你的要求。
RC522驱动使用一个switch{}解析命令，在里面添加case就可以了。非常方便。

4，为避免整个卡的所有扇区被误操作，驱动使用一个sect lock锁保护一些低端位置（0-10）扇区。
因为初学者很容易因为误操作把卡的扇区锁死，这样该扇区就废了。所以增加了这个功能。
当用户想要对这些这些扇区进行修改控制块（密码区，控制区）时，需要在结构体中设置解锁标志（0-》加锁，1-》解锁，默认加锁）。
当然你也可以修改一个宏定义CARD_LOCK_NSECT（默认是10）从而增加或减少保护的扇区数目，

具体使用可以看测试代码，print打印的信息文件。
我花了一周时间从单片机驱动上面移植过来的。
优化并大幅度简化了用户相关操作。让它更像一个标准linux 驱动，而不是使用单片机思想（眉毛胡子一把抓）编写的应用。
以下是控制结构体struct rc522_ioc_transfer{},
最后的两个__u8 id_buf[CARD_ID_SIZE]，__u8 txrx_buf[CARD_BLK_SIZE + CARD_PLUS_SIZE]，
不要改位置和大小，否则可能会出问题。
这是要复制到用户空间的数据区。
可以在__u8 keya_buf[CARD_KEYA_SIZE]以上的位置加成员。
struct rc522_ioc_transfer {
	__u32	speed_hz;
	__u32	pad;
	__u16	delay_usecs;
	__u8	bits_per_word;
	__u8	cs_change;

	enum rc522_ioc_type ioc_type;	/* io ctrl cmd : req, read, write... */
	enum rc522_key_type key_type;	/* key type: key a, key b, key a and b */
	
	enum rc522_sect_num sect_num;	/* sector number: 0~15 */
	enum rc522_blk_num blk_num;	/* block number: 0~4 */

	enum rc522_sect_lock sect_lock;	/* sect lock: 0=lock, 1=unlock */
	
	__u8	keya_buf[CARD_KEYA_SIZE];	/*key a space: 6 Bytes*/
	__u8	ctrl_buf[CARD_CTRL_SIZE];	/*ctrl space: 4 Bytes*/
	__u8	keyb_buf[CARD_KEYB_SIZE];	/*key b space: 6 Bytes*/
	
	__u8	id_buf[CARD_ID_SIZE];	/*id space: 4Bytes for auth card*/
	__u8	txrx_buf[CARD_BLK_SIZE + CARD_PLUS_SIZE]; /*w, r space: 16 + 4 Bytes*/
	
};
